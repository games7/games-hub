# games-hub

Games hub PoC witha monorepo setup using yarn workspaces

## Setup

For server dependencies you must run in root folder

```shell
yarn
```

Games also contain a `ui` folder where the client art of the game is deveoloped. To install dependencies on those UIs you should run.

```shell
yarn run setup-games
```

## Development

Start the server and game ui using the command

```shell
yarn start ${game-to-develop}
```

This command will run the server and UI concurrently so you can start adding new features to the game ui. (TODO: implement hot reload on the server)

## Build

```shell
yarn build
```

## Deploy

TBD (docker on AWS?)