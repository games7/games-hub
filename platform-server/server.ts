import * as express from "express";
import * as socketio from "socket.io";
import * as path from "path";
import { Player, Games } from 'platform-models';
import { WSEvents } from "./WebSocketEvents";

// TODO: this changes depending on env (DEV or PROD)
const clientFolder = "./dist/client";
const indexPage = "index.html";
const app = express();
//app.set("port", process.env.PORT || 3000);

let http = require("http").Server(app);

// set up socket.io and bind it to our http server.
let io = socketio(http);

let players: Player[] = [];

// workaround, to keep track of waiting players
let waitingPlayers: number = 0;

app.get("/", (req: any, res: any) => {
  res.sendFile(path.resolve(`${clientFolder}/${indexPage}`));
});

app.use(express.static(clientFolder));

app.use((req, res) => {
    res.sendFile(path.join(__dirname, `${clientFolder}/${indexPage}`));
  });

// whenever a user connects on port 3000 via
// a websocket, log that a user has connected
io.on(WSEvents.connection, (socket: socketio.Socket) => {
    players.push({id: socket.id, registered: false});

    // Greet and invite to pick a nickname
  
    socket.on(WSEvents.message, (message: any) => {
        console.log(message);
        socket.emit("message", `You just send: ${message}`);
    });

    socket.on(WSEvents.disconnect, () => {
        console.log(`${socket.id} got disconnected`);
        
        //remove from players array
    });

    socket.on(WSEvents.tictactoe, (message: any) => {
        console.log(`Tic tac toe message received: ${message.game}`);
        socket.emit(WSEvents.message, `tictactoe: ${message.playerName}`);
    });

    // TODO: Game routing
    socket.on(WSEvents.startGame, (message: any) => {

        switch (message.game){
            case Games.TicTacToe:
                console.log(`${message.playerName} joins ${message.game} queue`);
                HandleTicTacToeGameRequest(socket);
                break;
            case Games.Chess:
            case Games.Otello:
            default:
                socket.emit(WSEvents.message, `This game has not been developed yet`);
                break;
        }
    });
    

    socket.on(WSEvents.move, (message: any) => {
        console.log(`Tic tac toe message received: ${message.game}`);
        socket.emit(WSEvents.message, `tictactoe: ${message.playerName}`);
    });

});

const server = http.listen(3000, () => {
  console.log("listening on *:3000");
});

function HandleTicTacToeGameRequest(socket: socketio.Socket) {
    waitingPlayers++;
    if (waitingPlayers < 2) {
        socket.emit(WSEvents.message, `You joined the queue to play Tic Tac Toe!`);
        console.log("waitingPlayers", waitingPlayers);
    } else if (waitingPlayers === 2) {
        socket.emit(WSEvents.tictactoeRedirection);
        // TODO: player id harcoded!!!!
        socket.broadcast.to(players[0].id).emit(WSEvents.tictactoeRedirection);

        socket.emit(WSEvents.message, "Start the game!");
        socket.broadcast.to(players[0].id).emit(WSEvents.message, "Start the game!");
    } else if (waitingPlayers > 2) {
        socket.emit(WSEvents.message, "There is no more room for more players :(");
    }
}