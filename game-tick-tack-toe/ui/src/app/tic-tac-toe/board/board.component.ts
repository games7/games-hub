import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styles: []
})
export class BoardComponent implements OnInit {
  public squareValues: string[][] = [
    ['-','-','-'],
    ['-','-','-'],
    ['-','-','-']
  ];
 
  constructor() { }

  ngOnInit(): void {
  }

}
