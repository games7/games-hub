import { Component, OnInit } from '@angular/core';
import { WebsocketService } from './services/websocket.service';
import { WSEvents } from './shared/events';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'games';
  message: string;

  constructor(private wsService: WebsocketService, private route: Router) {}

  ngOnInit() {
    this.wsService.listen(WSEvents.message).subscribe(data => {
      this.message = data;
    });

    
    this.wsService.listen(WSEvents.tictactoeRedirection).subscribe(data => {
      this.route.navigate(['/tictactoe']);
    })
  }
}
