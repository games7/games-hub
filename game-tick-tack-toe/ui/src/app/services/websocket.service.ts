import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { WSEvents } from '../shared/events';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  socket: SocketIOClient.Socket;

  constructor() { 
    this.socket = io(environment.webSocketServer);
  }

  listen(eventName: WSEvents): Observable<any> {
    return new Observable( subscriber => {
      this.socket.on(eventName, data => {
        subscriber.next(data);
      });
    });
  }

  emit(eventName: WSEvents, data: any) {
    this.socket.emit(eventName, data);
  }
}
